// Mykyta Onipchenko   2034746

public class SubtractonacciSequence extends Sequence {
    private int num1;
    private int num2;

    public SubtractonacciSequence(int num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    @Override
    public int getTerm(int n) {
        validatePositive(n);

        if (n == 0) return this.num1;
        if (n == 1) return this.num2;

        // Need to calculate for other terms
        int[] terms = new int[n + 1];
        terms[0] = this.num1;
        terms[1] = this.num2;

        for (int i = 2; i <= n; i++) {
            int previous = terms[i - 2];
            int recent = terms[i - 1];

            terms[i] = previous - recent;
        }

        return terms[n];
    }
}
