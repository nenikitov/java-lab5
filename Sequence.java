// Mykyta Onipchenko   2034746
// Bogdan Ivan         2032824

public abstract class Sequence {
    public abstract int getTerm(int n);
    public void validatePositive(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("Only the positive terms are supported");
        }
    }
}
