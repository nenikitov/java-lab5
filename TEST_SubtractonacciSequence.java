import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class TEST_SubtractonacciSequence {
    @Test
    public void testConstructor() {
        SubtractonacciSequence sub1 = new SubtractonacciSequence(2, 3);
        SubtractonacciSequence sub2 = new SubtractonacciSequence(-2, -3);
        SubtractonacciSequence sub3 = new SubtractonacciSequence(2, -3);
        SubtractonacciSequence sub4 = new SubtractonacciSequence(-2, 3);
        SubtractonacciSequence sub5 = new SubtractonacciSequence(0, 3);
        SubtractonacciSequence sub6 = new SubtractonacciSequence(2, 0);
        SubtractonacciSequence sub7 = new SubtractonacciSequence(0, 0);
    }

    @Test
    public void testInvalidNumber(){
        try {
            SubtractonacciSequence sub = new SubtractonacciSequence(10, 20);
            sub.getTerm(-10);
            fail("Number should have been invalid");
        }
        catch(IllegalArgumentException e){
            // ok
        }
        catch(Exception e) {
            fail("Invalid exception has been thrown");
        }
    }

    @Test
    public void testPositiveNumber(){
        SubtractonacciSequence sub = new SubtractonacciSequence(2, 3);
        assertEquals(9, sub.getTerm(5));
    }

    @Test
    public void testNegativeNumber(){
        SubtractonacciSequence sub1 = new SubtractonacciSequence(-2, 3);
        assertEquals(21, sub1.getTerm(5));
        SubtractonacciSequence sub2 = new SubtractonacciSequence(2, -3);
        assertEquals(-21, sub2.getTerm(5));
        SubtractonacciSequence sub3 = new SubtractonacciSequence(-2, -3);
        assertEquals(-9, sub3.getTerm(5));
    }

    @Test
    public void testZeros(){
        SubtractonacciSequence sub5 = new SubtractonacciSequence(0, 3);
        assertEquals(15, sub5.getTerm(5));
        SubtractonacciSequence sub6 = new SubtractonacciSequence(2, 0);
        assertEquals(-6, sub6.getTerm(5));
        SubtractonacciSequence sub7 = new SubtractonacciSequence(0, 0);
        assertEquals(0, sub7.getTerm(5));
    }

    @Test
    public void testInitialValues(){
        SubtractonacciSequence sub8 = new SubtractonacciSequence(1, 2);
        assertEquals(1, sub8.getTerm(0));
        assertEquals(2, sub8.getTerm(1));
    }
}
