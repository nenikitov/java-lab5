import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TEST_FibonacciSequence {
    @Test
    public void testConstructor() {
        FibonacciSequence fib;

        fib = new FibonacciSequence( 1,  2);
        fib = new FibonacciSequence(-1,  2);
        fib = new FibonacciSequence( 1, -2);
        fib = new FibonacciSequence(-1, -2);
        fib = new FibonacciSequence( 0,  2);
        fib = new FibonacciSequence( 2,  0);
        fib = new FibonacciSequence( 0,  0);
    }

    @Test
    public void testInvalidTerm() {
        try {
            FibonacciSequence fib = new FibonacciSequence(1, 2);
            fib.getTerm(-10);
            fail("Should have thrown an exception");
        }
        catch (IllegalArgumentException e) {
            // All good, good type of exception
        }
        catch (Exception e) {
            fail("Wrong exception type thrown");
        }
    }

    @Test
    public void testTermPositives() {
        FibonacciSequence fib = new FibonacciSequence(1, 2);
        assertEquals(13, fib.getTerm(5));
    }

    @Test
    public void testInitialTerms() {
        FibonacciSequence fib = new FibonacciSequence(1, 2);
        assertEquals(1, fib.getTerm(0));
        assertEquals(2, fib.getTerm(1));
    }

    @Test
    public void testTermWithNegatives() {
        FibonacciSequence fib;

        fib = new FibonacciSequence(-1, 2);
        assertEquals(7, fib.getTerm(5));
        fib = new FibonacciSequence(1, -2);
        assertEquals(-7, fib.getTerm(5));
        fib = new FibonacciSequence(-1, -2);
        assertEquals(-13, fib.getTerm(5));
    }

    @Test
    public void testTermWithZeros() {
        FibonacciSequence fib;

        fib = new FibonacciSequence(0, 2);
        assertEquals(10, fib.getTerm(5));
        fib = new FibonacciSequence(2, 0);
        assertEquals(6, fib.getTerm(5));
        fib = new FibonacciSequence(0, 0);
        assertEquals(0, fib.getTerm(5));
    }
}
