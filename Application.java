// Mykyta Onipchenko   2034746
// Bogdan Ivan         2032824

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Input your sequences in the format of TYPE;NUM1;NUM2; where types are Fib or Sub");
        String seqText = scan.next();

        try {
            System.out.println("=== Here are your sequences ===");
            Sequence[] sequences = parse(seqText);
            print(sequences, 10);
        }
        catch (IllegalArgumentException e) {
            System.out.println("=== ERROR! Wrong format ===");
            System.out.println(e.getMessage());
        }

        scan.close();
    }

    public static void print(Sequence[] sequences, int n) {
        for (int i = 0; i < sequences.length; i++){
            System.out.println("Sequence #" + (i + 1));
            for (int j = 0; j < n; j++) {
                System.out.println("\t" + sequences[i].getTerm(j));
            }
        }
    }
    public static Sequence[] parse(String text) {
        // Because we implemented only subtractionacci and fibonacci, we only need to use 2 inputs

        String[] elements = text.split(";");

        if (elements.length % 3 != 0) {
            throw new IllegalArgumentException("Invalid number of inputs");
        }

        Sequence[] sequences = new Sequence[elements.length / 3];

        for (int i = 0; i < sequences.length; i++) {
            int num1;
            int num2;

            try {
                num1 = Integer.parseInt(elements[i * 3 + 1]);
                num2 = Integer.parseInt(elements[i * 3 + 2]);
            }
            catch (NumberFormatException e) {
                throw new IllegalArgumentException("Invalid number inputs");
            }

            switch(elements[i * 3]) {
                case "Fib":
                    sequences[i] = new FibonacciSequence(num1, num2);
                    break;
                case "Sub":
                    sequences[i] = new SubtractonacciSequence(num1, num2);
                    break;
                default:
                    throw new IllegalArgumentException("Invalid sequence name");
            }
        }

        return sequences;
    }
}
