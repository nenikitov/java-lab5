// Bogdan Ivan         2032824
public class FibonacciSequence extends Sequence {
    private int num1;
    private int num2;

    public FibonacciSequence(int num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    @Override
    public int getTerm(int n) {
        validatePositive(n);

        if (n == 0) {
            return this.num1;
        }
        if (n == 1) {
            return this.num2;
        }

        int[] seq = new int[n + 1];
        seq[0] = this.num1;
        seq[1] = this.num2;

        for(int i = 2; i < seq.length; i++){
            seq[i] = seq[i - 2] + seq[i - 1];
        }

        return seq[n];
    }
}
